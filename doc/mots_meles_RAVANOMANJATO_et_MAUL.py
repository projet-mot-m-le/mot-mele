from tkinter import*        #import necessaire pour la creation de toutes les fonctions
from tkinter import font
import tkinter as tk




class Appli:
    def __init__(self, fen):                                               #fonction pour creer le fenetre du niveau 1
        self.fen = fen
        self.fen.title("Mot-Meles niveau 1")                               #nom de la fenetre
        self.fen.geometry("1540x1050")                                     #dimentions de la fenetre (changable)
        self.fen['bg'] = "snow2"                                           #pour choisir une couleur
        self.fen.resizable(height=False,width=False)                       #pour modifier les dimentions



        label = Label(self.fen, text="𝕄𝕠𝕥 𝕄𝕖𝕝𝕖 ", font=("Double Struck",90,'italic bold'), fg="tan", bg="snow2")       # grand titre de la presentation de ma fenetre
        label.place(x="55", y="10")

        label = Label(self.fen, text=":Retrouve les mots cachés dans la grille ci-contre\n (sens de lecture horizontal et vertical),\n puis déchiffrer le code secret avec les lettres restantes (ecrire en Majuscule).", font=("Double Struck",12), fg="black", bg="snow2")    #consigne
        label.place(x="80", y="210")

        label = Label(self.fen, text="𝑪𝑶𝑵𝑺𝑰𝑮𝑵𝑬 :", font=("Double Struck",21,'italic bold'), fg="darkred", bg="snow2")    #consigne avec une ecriture et une tipologie specifique
        label.place(x="12", y="200")

        label = Label(self.fen, text="AGE \n\nSORT \n\nCHEZ \n\nFLEUR \n\nNEZ", font=("Double Struck",25,'italic bold'), fg="indianred", bg="snow2")  #mot a trouver
        label.place(x="100", y="320")

        label = Label(self.fen, text="TIR \n\nRIRE \n\nTARD \n\nLIRE \n\nGUIDE", font=("Double Struck",25,'italic bold'), fg="indianred", bg="snow2")
        label.place(x="400", y="320")




        def changeText():                               #fontion pour changer le texte sur un bouton en appuyant dessus
            if(btn['text']==''):
                btn['text']='OK'
            else:
                btn['text']=''


        def changeText1():
            if(btn1['text']==''):
                btn1['text']='OK'
            else:
                btn1['text']=''

        def changeText2():
            if(btn2['text']==''):
                btn2['text']='OK'
            else:
                btn2['text']=''

        def changeText3():
            if(btn3['text']==''):
                btn3['text']='OK'
            else:
                btn3['text']=''

        def changeText4():
            if(btn4['text']==''):
                btn4['text']='OK'
            else:
                btn4['text']=''

        def changeText5():
            if(btn5['text']==''):
                btn5['text']='OK'
            else:
                btn5['text']=''

        def changeText6():
            if(btn6['text']==''):
                btn6['text']='OK'
            else:
                btn6['text']=''

        def changeText7():
            if(btn7['text']==''):
                btn7['text']='OK'
            else:
                btn7['text']=''

        def changeText8():
            if(btn8['text']==''):
                btn8['text']='OK'
            else:
                btn8['text']=''

        def changeText9():
            if(btn9['text']==''):
                btn9['text']='OK'
            else:
                btn9['text']=''




        btn = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText)     #bouton qui permettent au joueur de valiter le mot trouver
        btn.place(x=70, y=325)
        btn1 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText1)
        btn1.place(x=70, y=405)
        btn2 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText2)
        btn2.place(x=70, y=480)
        btn3 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText3)
        btn3.place(x=70, y=555)
        btn4 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText4)
        btn4.place(x=70, y=630)
        btn5 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText5)
        btn5.place(x=370, y=325)
        btn6 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText6)
        btn6.place(x=370, y=405)
        btn7 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText7)
        btn7.place(x=370, y=480)
        btn8 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText8)
        btn8.place(x=370, y=555)
        btn9 = Button(self.fen, text='', width=2, height=1, bg="white", command=changeText9)
        btn9.place(x=370, y=630)


        user_Entry = Entry(fen, bg="white", font=("Verdana",32, "italic bold"), fg="Pink1")                     #céation d'une zone d'ecriture libre
        user = Label(fen, text = "CODE :", font=("Verdana",40, "italic bold"), fg="HotPink4", bg="snow2")       #texte Code sur la fentre
        user.place(x=50, y=720)
        user_Entry.place(x=275, y=740, width=200, height=40)


        def getEntry():               #fonction pour saisir le code rentrer
            res = user_Entry.get()
            print(res)
            if res=="NSI":                                                                                              # mettre une condition pour valider le code "nsi"
                fenetre = Tk()
                fenetre.geometry("1540x1050")                                                                           #dimentions de la fenetre (changable)
                fenetre.title("Bravo!!!")                                                                               #donner un nom a notre fenetre (changable)
                fenetre['bg'] = "pink"                                                                                  #PhotoImage(file = "conf.png") #pour choisir une couleur
                fenetre.resizable(height=False,width=False)                                                          #pour modifier les dimentions


                label = Label(fenetre, text="Bravo!!!!", font=("Verdana",150, "italic bold"), fg="white", bg="pink")     #mettre le texte bravo
                label.place(x=300, y=200)
                label = Label(fenetre, text="Vous avez reussi le niveau 1 !", font=("Verdana",30, "italic bold"), fg="white", bg="pink")     #mettre le texte bravo
                label.place(x=370, y=500)



                def root():
                    fenetre = Tk()
                    fenetre.geometry("1540x1050")                                                                           #dimentions de la fenetre (changable)
                    fenetre.title("Mot-mêlé niveau 2")                                                                               #donner un nom a notre fenetre (changable)
                    fenetre['bg'] = "snow2"                                                                                  #PhotoImage(file = "conf.png") #pour choisir une couleur
                    fenetre.resizable(height=False,width=False)

                    label = Label(fenetre, text="𝕄𝕠𝕥 𝕄𝕖𝕝𝕖 ", font=("Double Struck",90,'italic bold'), fg="tan", bg="snow2")       # grand titre de la presentation de ma fenetre
                    label.place(x="25", y="10")
                    label = Label(fenetre, text="Retrouve les mots cachés dans la grille ci-contre\n (sens de lecture horizontal et vertical),\n puis déchiffrer le code secret avec les lettres \nrestantes (ecrire en Majuscule).", font=("Double Struck",12), fg="black", bg="snow2")    #consigne
                    label.place(x="150", y="210")
                    label = Label(fenetre, text="𝑪𝑶𝑵𝑺𝑰𝑮𝑵𝑬 :", font=("Double Struck",21,'italic bold'), fg="darkred", bg="snow2")    #consigne avec une ecriture et une tipologie specifique
                    label.place(x="12", y="200")
                    label = Label(fenetre, text="PORTE \n\nJUGER \n\nTOP \n\nCIRE \n\nARBRES \n\nBRAS", font=("Double Struck",15,'italic bold'), fg="indianred", bg="snow2")  #mot a trouver
                    label.place(x="100", y="340")
                    label = Label(fenetre, text="JEU \n\nABIME \n\nLAMPE \n\nLOUP \n\nCHOC \n\nBACHE \n\nBUT", font=("Double Struck",15,'italic bold'), fg="indianred", bg="snow2")
                    label.place(x="300", y="340")

                    def changeText():                               #fontion pour changer le texte sur un bouton en appuyant dessus
                        if(btn['text']==''):
                            btn['text']='OK'
                        else:
                            btn['text']=''


                    def changeText1():
                        if(btn1['text']==''):
                            btn1['text']='OK'
                        else:
                            btn1['text']=''

                    def changeText2():
                        if(btn2['text']==''):
                            btn2['text']='OK'
                        else:
                            btn2['text']=''

                    def changeText3():
                        if(btn3['text']==''):
                            btn3['text']='OK'
                        else:
                            btn3['text']=''

                    def changeText4():
                        if(btn4['text']==''):
                            btn4['text']='OK'
                        else:
                            btn4['text']=''

                    def changeText5():
                        if(btn5['text']==''):
                            btn5['text']='OK'
                        else:
                            btn5['text']=''

                    def changeText6():
                        if(btn6['text']==''):
                            btn6['text']='OK'
                        else:
                            btn6['text']=''

                    def changeText7():
                        if(btn7['text']==''):
                            btn7['text']='OK'
                        else:
                            btn7['text']=''

                    def changeText8():
                        if(btn8['text']==''):
                            btn8['text']='OK'
                        else:
                            btn8['text']=''

                    def changeText9():
                        if(btn9['text']==''):
                            btn9['text']='OK'
                        else:
                            btn9['text']=''

                    def changeText10():
                        if(btn10['text']==''):
                            btn10['text']='OK'
                        else:
                            btn10['text']=''

                    def changeText11():
                        if(btn11['text']==''):
                            btn11['text']='OK'
                        else:
                            btn11['text']=''

                    def changeText12():
                        if(btn12['text']==''):
                            btn12['text']='OK'
                        else:
                            btn12['text']=''


                    btn = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText)
                    btn.place(x=70, y=335)
                    btn1 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText1)
                    btn1.place(x=70, y=385)
                    btn2 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText2)
                    btn2.place(x=70, y=435)
                    btn3 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText3)
                    btn3.place(x=70, y=480)
                    btn4 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText4)
                    btn4.place(x=70, y=530)
                    btn5 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText5)
                    btn5.place(x=70, y=580)
                    btn6 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText6)
                    btn6.place(x=260, y=335)
                    btn7 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText7)
                    btn7.place(x=260, y=385)
                    btn8 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText8)
                    btn8.place(x=260, y=435)
                    btn9 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText9)
                    btn9.place(x=260, y=480)
                    btn10 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText10)
                    btn10.place(x=260, y=530)
                    btn11 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText11)
                    btn11.place(x=260, y=580)
                    btn12 = Button(fenetre, text='', width=2, height=1, bg="white", command=changeText12)
                    btn12.place(x=260, y=630)


                    user_Entry = Entry(fenetre, bg="white", font=("Verdana",28, "italic bold"), fg="Pink1")                     #céation d'une zone d'ecriture libre
                    user = Label(fenetre, text = "CODE :", font=("Verdana",40, "italic bold"), fg="HotPink4", bg="snow2")       #texte Code sur la fentr
                    user.place(x=20, y=700)
                    user_Entry.place(x=245, y=720, width=200, height=40)


                    def getEntry():               #fonction pour saisir le code rentrer
                        res = user_Entry.get()
                        print(res)
                        if res=="ETUDIER":                                                                                              # mettre une condition pour valider le code "etudier"
                            fen = Tk()
                            fen.geometry("1540x1050")                                                                           #dimentions de la fenetre (changable)
                            fen.title("Bravo!!!")                                                                               #donner un nom a notre fenetre (changable)
                            fen['bg'] = "pink"                                                                                  #PhotoImage(file = "conf.png") #pour choisir une couleur
                            fen.resizable(height=False,width=False)                                                             #pour modifier les dimentions

                            label = Label(fen, text="Bravo!!!!", font=("Verdana",150, "italic bold"), fg="white", bg="pink")     #mettre le texte bravo
                            label.place(x=300, y=200)
                            label = Label(fen, text="Vous avez reussi le niveau 2 !", font=("Verdana",30, "italic bold"), fg="white", bg="pink")     #mettre le texte bravo
                            label.place(x=370, y=500)


                            fen.mainloop() # ecrire la fonction

                        else:
                            page = Tk()
                            page.geometry("1540x1050")                                                                           #dimentions de la fenetre (changable)
                            page.title("Dommage")                                                                               #donner un nom a notre fenetre (changable)
                            page['bg'] = "darkblue"                                                                                  #PhotoImage(file = "conf.png") #pour choisir une couleur
                            page.resizable(height=False,width=False)

                            label = Label(page, text="Dommage", font=("Verdana",150, "italic bold"), fg="white", bg="darkblue")     #mettre le texte bravo
                            label.place(x=250, y=200)
                            label = Label(page, text="Vous n'avez pas reussi le niveau 2 !", font=("Verdana",30, "italic bold"), fg="white", bg="darkblue")     #mettre le texte bravo
                            label.place(x=370, y=500)


                            recommencer = tk.Button(page, height=1, width=13, fg="darkblue", font=("Double Struck",20,'italic bold'), text="Recommencer", command=page.destroy)    #faire un bouton pour valider la saisie du code
                            recommencer.place(x=1250, y=750)


                            page.mainloop()


                    bouton = tk.Button(fenetre, height=1, width=10, text="Validé", command=getEntry)    #faire un bouton pour valider la saisie du code
                    bouton.place(x=455, y=730)



                    # céation des boutons des lettre avec la fonction change.color()
                    self.B = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color9(self.B))
                    self.B.place(x=545, y=15)
                    self.B0 = tk.Button(fenetre, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B0))
                    self.B0.place(x=650, y=15)
                    self.B1 = tk.Button(fenetre, text="S", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color12(self.B1))
                    self.B1.place(x=755, y=15)
                    self.B2 = tk.Button(fenetre, text="J", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color15(self.B2))
                    self.B2.place(x=860, y=15)
                    self.B3 = tk.Button(fenetre, text="L", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color7(self.B3))
                    self.B3.place(x=965, y=15)
                    self.B4 = tk.Button(fenetre, text="O", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color7(self.B4))
                    self.B4.place(x=1070, y=15)
                    self.B5 = tk.Button(fenetre, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color7(self.B5))
                    self.B5.place(x=1175, y=15)
                    self.B6 = tk.Button(fenetre, text="P", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color7(self.B6))
                    self.B6.place(x=1280, y=15)
                    self.B7 = tk.Button(fenetre, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B7))
                    self.B7.place(x=1385, y=15)
                    self.B8 = tk.Button(fenetre, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color9(self.B8))
                    self.B8.place(x=545, y=120)
                    self.B9 = tk.Button(fenetre, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color10(self.B9))
                    self.B9.place(x=650, y=120)
                    self.B10= tk.Button(fenetre, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color12(self.B10))
                    self.B10.place(x=755, y=120)
                    self.B11 = tk.Button(fenetre, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color15(self.B11))
                    self.B11.place(x=860, y=120)
                    self.B12 = tk.Button(fenetre, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B12))
                    self.B12.place(x=965, y=120)
                    self.B13 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color2(self.B13))
                    self.B13.place(x=1070, y=120)
                    self.B14 = tk.Button(fenetre, text="B", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color6(self.B14))
                    self.B14.place(x=1175, y=120)
                    self.B15 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color4(self.B15))
                    self.B15.place(x=1280, y=120)
                    self.B16= tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B16))
                    self.B16.place(x=1385, y=120)
                    self.B17= tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color9(self.B17))
                    self.B17.place(x=545, y=225)
                    self.B18 = tk.Button(fenetre, text="H", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color10(self.B18))
                    self.B18.place(x=650, y=225)
                    self.B19 = tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color12(self.B19))
                    self.B19.place(x=755, y=225)
                    self.B20 = tk.Button(fenetre, text="G", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color15(self.B20))
                    self.B20.place(x=860, y=225)
                    self.B21 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color5(self.B21))
                    self.B21.place(x=965, y=225)
                    self.B22 = tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color2(self.B22))
                    self.B22.place(x=1070, y=225)
                    self.B23 = tk.Button(fenetre, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color6(self.B23))
                    self.B23.place(x=1175, y=225)
                    self.B24 = tk.Button(fenetre, text="P", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color4(self.B24))
                    self.B24.place(x=1280, y=225)
                    self.B25= tk.Button(fenetre, text="B", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B25))
                    self.B25.place(x=1385, y=225)
                    self.B26 = tk.Button(fenetre, text="O", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color9(self.B26))
                    self.B26.place(x=545, y=330)
                    self.B27 = tk.Button(fenetre, text="O", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color10(self.B27))
                    self.B27.place(x=650, y=330)
                    self.B28 = tk.Button(fenetre, text="B", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color12(self.B28))
                    self.B28.place(x=755, y=330)
                    self.B29 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color15(self.B29))
                    self.B29.place(x=860, y=330)
                    self.B30 = tk.Button(fenetre, text="M", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color5(self.B30))
                    self.B30.place(x=965, y=330)
                    self.B31 = tk.Button(fenetre, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color2(self.B31))
                    self.B31.place(x=1070, y=330)
                    self.B32= tk.Button(fenetre, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color6(self.B32))
                    self.B32.place(x=1175, y=330)
                    self.B33 = tk.Button(fenetre, text="M", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color4(self.B33))
                    self.B33.place(x=1280, y=330)
                    self.B34= tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B34))
                    self.B34.place(x=1385, y=330)
                    self.B35= tk.Button(fenetre, text="P", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color9(self.B35))
                    self.B35.place(x=545, y=435)
                    self.B36 = tk.Button(fenetre, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color10(self.B36))
                    self.B36.place(x=650, y=435)
                    self.B37 = tk.Button(fenetre, text="D", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B37))
                    self.B37.place(x=755, y=435)
                    self.B38 = tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color15(self.B38))
                    self.B38.place(x=860, y=435)
                    self.B39 = tk.Button(fenetre, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color5(self.B39))
                    self.B39.place(x=965, y=435)
                    self.B40 = tk.Button(fenetre, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color2(self.B40))
                    self.B40.place(x=1070, y=435)
                    self.B41 = tk.Button(fenetre, text="H", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color6(self.B41))
                    self.B41.place(x=1175, y=435)
                    self.B42= tk.Button(fenetre, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color4(self.B42))
                    self.B42.place(x=1280, y=435)
                    self.B43= tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B43))
                    self.B43.place(x=1385, y=435)
                    self.B44= tk.Button(fenetre, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B44))
                    self.B44.place(x=545, y=540)
                    self.B45 = tk.Button(fenetre, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color14(self.B45))
                    self.B45.place(x=650, y=540)
                    self.B46 = tk.Button(fenetre, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color14(self.B46))
                    self.B46.place(x=755, y=540)
                    self.B47 = tk.Button(fenetre, text="B", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color14(self.B47))
                    self.B47.place(x=860, y=540)
                    self.B48 = tk.Button(fenetre, text="B", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color5(self.B48))
                    self.B48.place(x=965, y=540)
                    self.B49 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B49))
                    self.B49.place(x=1070, y=540)
                    self.B50 = tk.Button(fenetre, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color6(self.B50))
                    self.B50.place(x=1175, y=540)
                    self.B51 = tk.Button(fenetre, text="L", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color4(self.B51))
                    self.B51.place(x=1280, y=540)
                    self.B52= tk.Button(fenetre, text="S", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color8(self.B52))
                    self.B52.place(x=1385, y=540)
                    self.B53= tk.Button(fenetre, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color3(self.B53))
                    self.B53.place(x=545, y=645)
                    self.B54 = tk.Button(fenetre, text="O", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color3(self.B54))
                    self.B54.place(x=650, y=645)
                    self.B55 = tk.Button(fenetre, text="P", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color3(self.B55))
                    self.B55.place(x=755, y=645)
                    self.B56 = tk.Button(fenetre, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B56))
                    self.B56.place(x=860, y=645)
                    self.B57 = tk.Button(fenetre, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color5(self.B57))
                    self.B57.place(x=965, y=645)
                    self.B58 = tk.Button(fenetre, text="J", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color11(self.B58))
                    self.B58.place(x=1070, y=645)
                    self.B59 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color11(self.B59))
                    self.B59.place(x=1175, y=645)
                    self.B60 = tk.Button(fenetre, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color11(self.B60))
                    self.B60.place(x=1280, y=645)
                    self.B61 = tk.Button(fenetre, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",8,'italic bold'), command=lambda: self.change_color(self.B61))
                    self.B61.place(x=1385, y=645)



                    fenetre.mainloop()                                                             #pour modifier les dimentions




                suivant = tk.Button(fenetre, height=1, width=13, fg="darksalmon", font=("Double Struck",20,'italic bold'), text="Suivant", command=root)
                suivant.place(x=1250, y=690)





                fenetre.mainloop()




            else:               #si le code "nsi" n'est pas rentrer


                RD = Tk()
                RD.geometry("1540x1050")                                                                           #dimentions de la fenetre (changable)
                RD.title("Dommage")                                                                               #donner un nom a notre fenetre (changable)
                RD['bg'] = "darkblue"                                                                                  #PhotoImage(file = "conf.png") #pour choisir une couleur
                RD.resizable(height=False,width=False)

                label = Label(RD, text="Dommage", font=("Verdana",150, "italic bold"), fg="white", bg="darkblue")     #mettre le texte bravo
                label.place(x=250, y=200)
                label = Label(RD, text="Vous n'avez pas reussi le niveau 1 !", font=("Verdana",30, "italic bold"), fg="white", bg="darkblue")     #mettre le texte bravo
                label.place(x=370, y=500)


                recommencer = tk.Button(RD, height=1, width=13, fg="darkblue", font=("Double Struck",20,'italic bold'), text="Recommencer", command=RD.destroy)    #faire un bouton pour valider la saisie du code
                recommencer.place(x=1250, y=750)


                RD.mainloop()


        bouton = tk.Button(self.fen, height=1, width=10, text="Validé", command=getEntry)    #faire un bouton pour valider la saisie du code
        bouton.place(x=490, y=750)


        # céation des boutons des lettre avec la fonction change.color()
        self.B0 = tk.Button(self.fen, text="S", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color(self.B0))
        self.B0.place(x=650, y=60)
        self.B1 = tk.Button(self.fen, text="D", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color2(self.B1))
        self.B1.place(x=755, y=60)
        self.B2 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color4(self.B2))
        self.B2.place(x=860, y=60)
        self.B3 = tk.Button(self.fen, text="D", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color4(self.B3))
        self.B3.place(x=965, y=60)
        self.B4 = tk.Button(self.fen, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color4(self.B4))
        self.B4.place(x=1070, y=60)
        self.B5 = tk.Button(self.fen, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color4(self.B5))
        self.B5.place(x=1175, y=60)
        self.B6 = tk.Button(self.fen, text="G", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color4(self.B6))
        self.B6.place(x=1280, y=60)
        self.B7 = tk.Button(self.fen, text="C", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color3(self.B7))
        self.B7.place(x=650, y=180)
        self.B8 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color2(self.B8))
        self.B8.place(x=755, y=180)
        self.B9 = tk.Button(self.fen, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color(self.B9))
        self.B9.place(x=860, y=180)
        self.B10 = tk.Button(self.fen, text="Z", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color8(self.B10))
        self.B10.place(x=965, y=180)
        self.B11 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color8(self.B11))
        self.B11.place(x=1070, y=180)
        self.B12 = tk.Button(self.fen, text="N", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color8(self.B12))
        self.B12.place(x=1175, y=180)
        self.B13 = tk.Button(self.fen, text="F", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color10(self.B13))
        self.B13.place(x=1280, y=180)
        self.B14 = tk.Button(self.fen, text="H", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color3(self.B14))
        self.B14.place(x=650, y=300)
        self.B15 = tk.Button(self.fen, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color2(self.B15))
        self.B15.place(x=755, y=300)
        self.B16 = tk.Button(self.fen, text="A", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color7(self.B16))
        self.B16.place(x=860, y=300)
        self.B17 = tk.Button(self.fen, text="G", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color7(self.B17))
        self.B17.place(x=965, y=300)
        self.B18 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color7(self.B18))
        self.B18.place(x=1070, y=300)
        self.B19 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color5(self.B19))
        self.B19.place(x=1175, y=300)
        self.B20 = tk.Button(self.fen, text="L", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color10(self.B20))
        self.B20.place(x=1280, y=300)
        self.B21 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color3(self.B21))
        self.B21.place(x=650, y=420)
        self.B22 = tk.Button(self.fen, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color2(self.B22))
        self.B22.place(x=755, y=420)
        self.B23 = tk.Button(self.fen, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color6(self.B23))
        self.B23.place(x=860, y=420)
        self.B24 = tk.Button(self.fen, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color6(self.B24))
        self.B24.place(x=965, y=420)
        self.B25 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color6(self.B25))
        self.B25.place(x=1070, y=420)
        self.B26 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color5(self.B26))
        self.B26.place(x=1175, y=420)
        self.B27 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color10(self.B27))
        self.B27.place(x=1280, y=420)
        self.B28 = tk.Button(self.fen, text="Z", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color3(self.B28))
        self.B28.place(x=650, y=540)
        self.B29 = tk.Button(self.fen, text="T", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color9(self.B29))
        self.B29.place(x=755, y=540)
        self.B30 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color9(self.B30))
        self.B30.place(x=860, y=540)
        self.B31 = tk.Button(self.fen, text="O", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color9(self.B31))
        self.B31.place(x=965, y=540)
        self.B32 = tk.Button(self.fen, text="S", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color9(self.B32))
        self.B32.place(x=1070, y=540)
        self.B33 = tk.Button(self.fen, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color5(self.B33))
        self.B33.place(x=1175, y=540)
        self.B34 = tk.Button(self.fen, text="U", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color10(self.B34))
        self.B34.place(x=1280, y=540)
        self.B35 = tk.Button(self.fen, text="L", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color11(self.B35))
        self.B35.place(x=650, y=660)
        self.B36 = tk.Button(self.fen, text="I", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color11(self.B36))
        self.B36.place(x=755, y=660)
        self.B37 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color11(self.B37))
        self.B37.place(x=860, y=660)
        self.B38 = tk.Button(self.fen, text="E", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color11(self.B38))
        self.B38.place(x=965, y=660)
        self.B39 = tk.Button(self.fen, text="N", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color(self.B39))
        self.B39.place(x=1070, y=660)
        self.B40 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color5(self.B40))
        self.B40.place(x=1175, y=660)
        self.B41 = tk.Button(self.fen, text="R", width=13, height=7, fg="black", bg="white", font=("Double Struck",9,'italic bold'), command=lambda: self.change_color10(self.B41))
        self.B41.place(x=1280, y=660)






    def change_color(self, button):   # Change la couleur du texte du bouton cliqué en noire
        button.config(fg="black")

    def change_color2(self, button):  # Change la couleur du texte du bouton cliqué en bleu
        button.config(fg="blue")

    def change_color3(self, button):  # Change la couleur du texte du bouton cliqué en violet
        button.config(fg="purple")

    def change_color4(self, button):  # Change la couleur du texte du bouton cliqué en jaune
        button.config(fg="yellow")

    def change_color5(self, button):  # Change la couleur du texte du bouton cliqué en orange
        button.config(fg="orange")

    def change_color6(self, button):  # Change la couleur du texte du bouton cliqué en vert
        button.config(fg="green")

    def change_color7(self, button):  # Change la couleur du texte du bouton cliqué en rouge
        button.config(fg="red")

    def change_color8(self, button):  # Change la couleur du texte du bouton cliqué en vert olive
        button.config(fg="oliveDrab4")

    def change_color9(self, button):  # Change la couleur du texte du bouton cliqué en marron
        button.config(fg="maroon")

    def change_color10(self, button):  # Change la couleur du texte du bouton cliqué en rose
        button.config(fg="pink")

    def change_color11(self, button):  # Change la couleur du texte du bouton cliqué en violet4
        button.config(fg="purple4")

    def change_color12(self, button):  # Change la couleur du texte du bouton cliqué en cyan
        button.config(fg="cyan")

    def change_color15(self, button):  # Change la couleur du texte du bouton cliqué en violet
        button.config(fg="hot pink")

    def change_color14(self, button):  # Change la couleur du texte du bouton cliqué en lime
        button.config(fg="lime")




def main():                        #afficher la fentre
    fenetre = tk.Tk()
    app = Appli(fenetre)
    fenetre.mainloop()

if __name__ == "__main__":
    main()



